
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 5

// Bolts/AppLinks
#define COCOAPODS_POD_AVAILABLE_Bolts_AppLinks
#define COCOAPODS_VERSION_MAJOR_Bolts_AppLinks 1
#define COCOAPODS_VERSION_MINOR_Bolts_AppLinks 1
#define COCOAPODS_VERSION_PATCH_Bolts_AppLinks 5

// Bolts/Tasks
#define COCOAPODS_POD_AVAILABLE_Bolts_Tasks
#define COCOAPODS_VERSION_MAJOR_Bolts_Tasks 1
#define COCOAPODS_VERSION_MINOR_Bolts_Tasks 1
#define COCOAPODS_VERSION_PATCH_Bolts_Tasks 5

// MHTextField
#define COCOAPODS_POD_AVAILABLE_MHTextField
#define COCOAPODS_VERSION_MAJOR_MHTextField 0
#define COCOAPODS_VERSION_MINOR_MHTextField 0
#define COCOAPODS_VERSION_PATCH_MHTextField 4

// Mantle
#define COCOAPODS_POD_AVAILABLE_Mantle
#define COCOAPODS_VERSION_MAJOR_Mantle 2
#define COCOAPODS_VERSION_MINOR_Mantle 0
#define COCOAPODS_VERSION_PATCH_Mantle 0

// Mantle/extobjc
#define COCOAPODS_POD_AVAILABLE_Mantle_extobjc
#define COCOAPODS_VERSION_MAJOR_Mantle_extobjc 2
#define COCOAPODS_VERSION_MINOR_Mantle_extobjc 0
#define COCOAPODS_VERSION_PATCH_Mantle_extobjc 0

// Parse
#define COCOAPODS_POD_AVAILABLE_Parse
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.7.2.1.

