//
//  NSDate+Utility.h
//  Tripper
//
//  Created by joehsieh on 3/20/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utility)
+ (NSDate *)convertStringToDate:(NSString *)inString;
+ (NSString *)convertDateToString:(NSDate *)inDate;
+ (NSArray *)datesBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
- (NSDate *)dateAfterdays:(NSUInteger)inDays;
@end
