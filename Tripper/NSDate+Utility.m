//
//  NSDate+Utility.m
//  Tripper
//
//  Created by joehsieh on 3/20/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "NSDate+Utility.h"

@implementation NSDate (Utility)

- (NSDate *)dateAfterdays:(NSUInteger)inDays
{
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:inDays];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    return [gregorian dateByAddingComponents:components toDate:self options:0];
}

+ (NSDate *)convertStringToDate:(NSString *)inString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter dateFromString:inString];
}

+ (NSString *)convertDateToString:(NSDate *)inDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:inDate];
}

+ (NSArray *)datesBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0 ; i <= [difference day] ; i ++) {
        [dates addObject:[fromDate dateAfterdays:i]];
    }
    
    return dates;
}

@end
