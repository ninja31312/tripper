//
//  TPAPI.h
//  Tripper
//
//  Created by joehsieh on 3/15/14.
//  Copyright (c) 2014 NJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Bolts/BFTask.h>

@interface TPAPI : NSObject <NSURLSessionDelegate>

+ (instancetype)sharedAPI;

- (BFTask *)retrieveHotArticlesForUser:(NSString *)inUserName;
- (BFTask *)createArticleWith:(NSDictionary *)inDictionary;
- (BFTask *)searchArticlesByKeyWord:(NSString *)inKeyword;
@end
