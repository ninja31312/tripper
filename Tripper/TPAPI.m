//
//  TPAPI.m
//  Tripper
//
//  Created by joehsieh on 3/16/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPAPI.h"
#import <Bolts/BFTaskCompletionSource.h>

static const NSString *rootPath = @"http://emma.pixnet.cc";
static const NSTimeInterval kDefaultTimeout = 60.0;

NSString * const cliend_id = @"d1dfd15ddcf67cb95f26144249640a38";
NSString * const secret = @"b8a1d311bba1003cb2664de6809fac1e";

NSString * const accessToken = @"ae3a3a9e5bb0e7c1a902f726b5586598";
NSString * const accessTokenSecret = @"4c335a087a0b14b7d0237ca2a6b519bd";

NSString * const accountType = @"joeTrip";
NSString * const username = @"yseric";
NSString * const password = @"ericeric";

@interface TPAPI()
@property(nonatomic, strong) NSURLSession *URLSession;
@property(nonatomic, strong) NSOperationQueue *operationQueue;
@end

@implementation TPAPI

+ (instancetype)sharedAPI
{
    static TPAPI *sharedAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.operationQueue = [[NSOperationQueue alloc] init];
		NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
		config.allowsCellularAccess = YES;
		config.timeoutIntervalForRequest = kDefaultTimeout;
		self.URLSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:operationQueue];
    }
    return self;
}

- (NSMutableURLRequest *)_createRequestWithPath:(NSString *)inPath parameterMap:(NSDictionary *)inParameters
{
    NSMutableString *parameterString = [[NSMutableString alloc] initWithString:@""];
    if (inParameters) {
        NSUInteger i = 0;
        for (NSString *key in inParameters.allKeys) {
            [parameterString appendFormat:i ? @"&%@=%@" : @"?%@=%@", key, inParameters[key]];
            i++;
        }
    }
    NSString *path = [NSString stringWithFormat:@"%@/%@%@",rootPath, inPath, parameterString];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
}

- (BFTask *)retrieveHotArticlesForUser:(NSString *)inUserName
{
    NSURLRequest *request = [self _createRequestWithPath:@"blog/articles/hot" parameterMap:@{@"user":inUserName}];
    
    BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
    [[URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            [source setError:error];
            return;
        }
        NSError *e = nil;
		id JSONResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&e];
        [source setResult:JSONResponse];
    }] resume];
    return source.task;
}

- (BFTask *)createArticleWith:(NSDictionary *)inDictionary
{
    NSMutableURLRequest *request = [self _createRequestWithPath:@"blog/articles" parameterMap:@{@"title":@"testTitle", @"body":@"testBody"}];
    [request setHTTPMethod:@"POST"];
    BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
    [[URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            [source setError:error];
            return;
        }
        NSError *e = nil;
		id JSONResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&e];
        [source setResult:JSONResponse];
    }] resume];
    return source.task;
}

- (BFTask *)searchArticlesByKeyWord:(NSString *)inKeyword
{
    NSURLRequest *request = [self _createRequestWithPath:@"blog/articles/search" parameterMap:@{@"key":inKeyword, @"per_page":@(20)}];

    BFTaskCompletionSource *source = [BFTaskCompletionSource taskCompletionSource];
    [[URLSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            [source setError:error];
            return;
        }
        NSError *e = nil;
		id JSONResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&e];
        [source setResult:JSONResponse];
    }] resume];
    return source.task;
}

@synthesize URLSession;
@synthesize operationQueue;
@end

