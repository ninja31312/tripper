//
//  TPActivitiesViewController.h
//  Tripper
//
//  Created by joehsieh on 3/19/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPAddActivityViewController.h"
#import "TPEditActivityViewController.h"

@interface TPActivitiesViewController : UIViewController <TPAddActivityViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, TPEditActivityViewControllerDelegate>
- (instancetype)initWithEvent:(TPEvent *)inEvent;
@end
