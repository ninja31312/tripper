//
//  TPActivitiesViewController.m
//  Tripper
//
//  Created by joehsieh on 3/19/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPActivitiesViewController.h"
#import "SDSegmentedControl.h"
#import "NSDate+Utility.h"
#import "TPAddActivityViewController.h"
#import "TPActivity.h"
#import <Parse/PFObject.h>
#import <Parse/PFQuery.h>

static NSString *CellIdentifier = @"Cell";

@interface TPActivitiesViewController ()
@property (nonatomic, strong) TPEvent *event;
@property (nonatomic, strong) NSArray *activities;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) SDSegmentedControl *segmentControl;
@property (nonatomic, strong) NSArray *dates;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) NSIndexPath *indexPath;
@end

@implementation TPActivitiesViewController

- (instancetype)initWithEvent:(TPEvent *)inEvent
{
    self = [super init];
    if(self) {
        self.event = inEvent;
    }
    return self;
}

- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    view.backgroundColor = [UIColor whiteColor];
    self.view = view;
    CGRect rect = CGRectMake(0, 64.0, CGRectGetWidth(self.view.frame), 64.0);
    self.segmentControl = [[SDSegmentedControl alloc] init];
    [segmentControl addTarget:self action:@selector(_segmentSelectionChanged:) forControlEvents:UIControlEventValueChanged];
    segmentControl.frame = rect;
    segmentControl.scrollView.contentSize = CGSizeMake(CGRectGetWidth(segmentControl.scrollView.bounds), CGRectGetHeight(segmentControl.frame));
    NSDate *startDate = [NSDate convertStringToDate:event.startDate];
    NSDate *endDate = [NSDate convertStringToDate:event.endDate];
    self.dates = [NSDate datesBetweenDate:startDate andDate:endDate];
    for (NSDate *date in dates) {
        NSString *dateString = [NSDate convertDateToString:date];
        [segmentControl insertSegmentWithTitle:dateString atIndex:[dates indexOfObject:date] animated:YES];
    }
    
    [self.view addSubview:segmentControl];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(segmentControl.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(segmentControl.frame) - 64.0)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(_addActivity:)];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    self.activities = [[NSArray alloc] init];
    
    self.indicator = [[UIActivityIndicatorView alloc] init];
    indicator.color = [UIColor whiteColor];
    indicator.backgroundColor = [UIColor grayColor];
    
    CGFloat w = 44.0;
    CGFloat h = 44.0;
    indicator.frame = CGRectMake((CGRectGetWidth(self.view.frame) - w) / 2.0, (CGRectGetHeight(self.view.frame) - w) / 2.0, w, h);
    [self.tableView addSubview:indicator];
    [indicator startAnimating];
    [self _loadDataFromParse];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)_loadDataFromParse
{
    PFQuery *query = [PFQuery queryWithClassName:@"TPActivity"];
    NSDate *date = dates[segmentControl.selectedSegmentIndex];
    [query whereKey:@"eventId" equalTo:event.objectId];
    [query whereKey:@"date" equalTo:[NSDate convertDateToString:date]];
    [query orderByAscending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            NSLog(@"Error: %@", errorString);
            return;
        }
        NSMutableArray *result = [[NSMutableArray alloc] init];
        for (PFObject *item in objects) {
            NSString *objectId = item.objectId;
            TPActivity *activity = [TPActivity createActivityWithDictionary:@{@"title":item[@"activityName"], @"note":item[@"note"]?item[@"note"]:@"", @"objectId":objectId, @"link":item[@"link"]?item[@"link"]:@""}];
            [result addObject:activity];
        }
        self.activities = result;
        [self.tableView reloadData];
        [indicator stopAnimating];
    }];
}

#pragma mark TPAddActivityViewControllerDelegate

- (void)addActivityViewControllerDidComple:(TPAddActivityViewController *)inViewController activities:(NSArray *)inActivities
{
    [indicator startAnimating];
    [self dismissViewControllerAnimated:YES completion:^{
        NSDate *date = dates[segmentControl.selectedSegmentIndex];
        for (TPActivity *activity in inActivities) {
            PFObject *item = [PFObject objectWithClassName:@"TPActivity"];
            item[@"eventId"] = event.objectId;
            item[@"date"] = [NSDate convertDateToString:date];
            item[@"activityName"] = activity.title;
            item[@"link"] = activity.link;
#warning block main thread
            [item save];
        }
        // Becaue new added activity has no objectId, we must reload from Parse again.
        [self _loadDataFromParse];
    }];
}

- (void)addActivityViewControllerDidCancel:(TPAddActivityViewController *)inViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TPEditActivityViewControllerDelegate

- (void)editActivityViewControllerDidComple:(TPEditActivityViewController *)inViewController activity:(TPActivity *)inActivity
{
    [indicator startAnimating];
    NSDate *date = dates[segmentControl.selectedSegmentIndex];
    
    PFQuery *query = [PFQuery queryWithClassName:@"TPActivity"];
    
    [query getObjectInBackgroundWithId:inActivity.objectId block:^(PFObject *item, NSError *error) {
        item[@"eventId"] = event.objectId;
        item[@"date"] = [NSDate convertDateToString:date];
        item[@"activityName"] = inActivity.title;
        item[@"note"] = inActivity.note;
#warning block main thread
        [item save];
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[_indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        [indicator stopAnimating];
        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.activities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    TPActivity *activity = self.activities[indexPath.row];
    cell.textLabel.text = activity.title;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPActivity *activity = self.activities[indexPath.row];
    TPEditActivityViewController *vc = [[TPEditActivityViewController alloc] initWithActivity:activity];
    vc.delegate = self;
    _indexPath = indexPath;
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Action

- (void)_addActivity:(id)sender
{
    TPAddActivityViewController *vc = [[TPAddActivityViewController alloc] initWithEvent:self.event];
    vc.delegate = self;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)_segmentSelectionChanged:(id)sender
{
    [indicator startAnimating];
    [self _loadDataFromParse];
}

#pragma mark- Properties

@synthesize tableView;
@synthesize segmentControl;
@synthesize event;
@synthesize activities;
@synthesize dates;
@synthesize indicator;
@end
