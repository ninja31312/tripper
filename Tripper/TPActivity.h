//
//  TPActivity.h
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "MTLModel.h"

@interface TPActivity : MTLModel
+ (TPActivity *)createActivityWithDictionary:(NSDictionary *)inDict;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *note;
@property (atomic, assign) BOOL isChecked;
@end
