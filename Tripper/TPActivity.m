//
//  TPActivity.m
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPActivity.h"

@implementation TPActivity
+ (TPActivity *)createActivityWithDictionary:(NSDictionary *)inDict
{
    TPActivity *activity = [[TPActivity alloc] init];
    activity.title = inDict[@"title"];
    activity.link = inDict[@"link"];
    activity.note = inDict[@"note"];
    activity.objectId = inDict[@"objectId"];
    return activity;
}
@end
