#import <UIKit/UIKit.h>
@class TPActivityCell;

@protocol TPActivityCellDelegate <NSObject>
- (void)TPActivityCell:(TPActivityCell *)inCell didClickCheckButtonAtIndexPath:(NSIndexPath *)inIndexPath;
@end

@interface TPActivityCell : UITableViewCell
@property (assign, nonatomic, getter = isCheck) BOOL check;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (assign, nonatomic) id <TPActivityCellDelegate> tableViewCellDelegate;
@end
