#import "TPActivityCell.h"

@interface TPActivityCell()
@property (strong, nonatomic) UIButton *checkButton;
@end

@implementation TPActivityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];

	if (self) {
		self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.checkButton addTarget:self action:@selector(_attendingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        CGRect rect = self.checkButton.frame;
        rect.origin.x = 10.0;
        rect.origin.y = 5.0;
        rect.size.width = 44.0;
        rect.size.height = 44.0;
        self.checkButton.frame = rect;
		[self addSubview:self.checkButton];
	}
	return self;
}

- (void)_attendingButtonClicked:(id)sender
{
	if (self.tableViewCellDelegate && self.indexPath) {
		[self.tableViewCellDelegate TPActivityCell:self didClickCheckButtonAtIndexPath:self.indexPath];
	}
}

- (void)layoutSubviews
{
	[super layoutSubviews];

	CGRect contentRect = self.contentView.bounds;

	CGFloat constrainTextLabelWidth = CGRectGetWidth(self.frame) - CGRectGetWidth(checkButton.frame) - 10.0;
	self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
	self.textLabel.numberOfLines = 1;
	self.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
	CGFloat textLabelX = CGRectGetMaxX(self.checkButton.frame) + 10.0;
	CGFloat textLabelY = (CGRectGetHeight(self.frame) - CGRectGetHeight(self.textLabel.frame)) / 2.0;
	self.textLabel.frame = CGRectMake(textLabelX, textLabelY, constrainTextLabelWidth, CGRectGetHeight(self.textLabel.bounds));

	self.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0];
	self.detailTextLabel.numberOfLines = 1;
	self.detailTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
	[self.detailTextLabel sizeToFit];
	CGFloat detailTextLabelX = CGRectGetMaxX(self.textLabel.frame);
	CGFloat detailTextLabelY = (CGRectGetHeight(self.frame) - CGRectGetHeight(self.detailTextLabel.frame)) / 2.0;
	CGFloat detailTextLabelWidth = CGRectGetWidth(contentRect) - detailTextLabelX;;
    self.detailTextLabel.frame = CGRectMake(detailTextLabelX, detailTextLabelY, detailTextLabelWidth, CGRectGetHeight(self.detailTextLabel.bounds));
}

- (void)setCheck:(BOOL)inCheck
{
	_check = inCheck;
	NSString *imageName = _check ? @"check" : @"uncheck";
	UIImage *checkButtonImage = [UIImage imageNamed:imageName];
	[self.checkButton setImage:checkButtonImage forState:UIControlStateNormal];

	CGRect checkButtonFrame;
	checkButtonFrame = CGRectMake(0, 0, checkButtonImage.size.width * 2, 44.0);
	self.checkButton.frame = checkButtonFrame;
}


- (void)prepareForReuse
{
    [super prepareForReuse];
	self.indexPath = nil;
	[self.checkButton setImage:nil forState:UIControlStateNormal];
	self.tableViewCellDelegate = nil;
	self.backgroundColor = [UIColor whiteColor];
}

@synthesize checkButton;
@end
