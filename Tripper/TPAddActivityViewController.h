//
//  TPAddActivityViewController.h
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPActivityCell.h"
#import "TPEvent.h"

@class TPAddActivityViewController;

@protocol TPAddActivityViewControllerDelegate <NSObject>
- (void)addActivityViewControllerDidComple:(TPAddActivityViewController *)inViewController activities:(NSArray *)inActivities;
- (void)addActivityViewControllerDidCancel:(TPAddActivityViewController *)inViewController;
@end

@interface TPAddActivityViewController : UITableViewController <TPActivityCellDelegate>
- (instancetype)initWithEvent:(TPEvent *)inEvent;
@property (nonatomic, assign) id <TPAddActivityViewControllerDelegate> delegate;
@end
