//
//  TPAddActivityViewController.m
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPAddActivityViewController.h"
#import "TPAPI.h"
#import "TPActivity.h"
#import <Parse/PFObject.h>
#import "TPWebViewController.h"

static NSString *CellIdentifier = @"Cell";

@interface TPAddActivityViewController ()
@property (nonatomic, strong) TPEvent *event;
@property (nonatomic, strong) NSArray *activities;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@end

@implementation TPAddActivityViewController

- (instancetype)initWithEvent:(TPEvent *)inEvent
{
    self = [super init];
    if (self) {
        self.event = inEvent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSNumber *(^_isContainKeyword) (NSString *) = ^NSNumber* (NSString *inKeyword) {
        NSArray *keywords = @[@"遊", @"玩", @"吃", @"美食"];
        for(NSString *keyword in keywords) {
            if([inKeyword rangeOfString:keyword].location != NSNotFound) {
                return @(YES);
            }
        }
        return @(NO);
    };
    
    self.indicator = [[UIActivityIndicatorView alloc] init];
    indicator.color = [UIColor whiteColor];
    indicator.backgroundColor = [UIColor grayColor];
    
    CGFloat w = 44.0;
    CGFloat h = 44.0;
    indicator.frame = CGRectMake((CGRectGetWidth(self.view.frame) - w) / 2.0, (CGRectGetHeight(self.view.frame) - w) / 2.0, w, h);
    [self.view addSubview:indicator];
    
    [indicator startAnimating];
    
    [[[TPAPI sharedAPI] searchArticlesByKeyWord:self.event.eventName] continueWithBlock:^id(BFTask *task) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *result = task.result;
            NSMutableArray *mutableActivities = [[NSMutableArray alloc] init];
            for(NSDictionary *item in result[@"articles"]) {
                if([_isContainKeyword(item[@"site_category"]) boolValue] || [_isContainKeyword(item[@"category"]) boolValue]||
                   [_isContainKeyword(item[@"title"]) boolValue]) {
                    [mutableActivities addObject:[TPActivity createActivityWithDictionary:@{@"title":item[@"title"], @"link":item[@"link"]}]];
                }
            }
            self.activities = mutableActivities;
            [self.tableView reloadData];
            [indicator stopAnimating];
        });
        return task;
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.tableView registerClass:[TPActivityCell class] forCellReuseIdentifier:CellIdentifier];
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(_dismissPresentedViewController:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(_addActivities:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.activities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.tableViewCellDelegate = self;
    cell.indexPath = indexPath;
    TPActivity *activity = self.activities[indexPath.row];
    cell.textLabel.text = activity.title;
    cell.check = activity.isChecked;
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPWebViewController *vc = [[TPWebViewController alloc] init];
    TPActivity *activity = self.activities[indexPath.row];
    [vc loadURL:[NSURL URLWithString:activity.link]];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - TPActivityCellDelegate

- (void)TPActivityCell:(TPActivityCell *)inCell didClickCheckButtonAtIndexPath:(NSIndexPath *)inIndexPath
{
    TPActivity *activity = self.activities[inIndexPath.row];
    activity.isChecked = !activity.isChecked;
    inCell.check = !activity.isChecked;
    [self.tableView beginUpdates];
	[self.tableView reloadRowsAtIndexPaths:@[inIndexPath] withRowAnimation:UITableViewRowAnimationNone];
	[self.tableView endUpdates];
}

#pragma mark - Actions

- (void)_dismissPresentedViewController:(id)sender
{
    if (delegate) {
        [delegate addActivityViewControllerDidCancel:self];
    }
}

- (void)_addActivities:(id)sender
{
    NSMutableArray *checkedActivities = [[NSMutableArray alloc] init];
    for (TPActivity *activity in activities) {
        if (activity.isChecked) {
            [checkedActivities addObject:activity];
        }
    }
    
    if (delegate) {
        [delegate addActivityViewControllerDidComple:self activities:checkedActivities];
    }
}

#pragma mark - Properties

@synthesize delegate;
@synthesize activities;
@synthesize indicator;
@end
