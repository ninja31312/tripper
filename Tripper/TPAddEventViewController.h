//
//  TPAddEventViewController.h
//  Tripper
//
//  Created by joehsieh on 3/17/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TPAddEventViewController;

@protocol TPAddEventViewControllerDelegate <NSObject>
- (void)addEventViewControllerDidComple:(TPAddEventViewController *)inViewController;
- (void)addEventViewControllerDidCancel:(TPAddEventViewController *)inViewController;
@end
@interface TPAddEventViewController : UIViewController

@property (nonatomic, assign) id <TPAddEventViewControllerDelegate> delegate;
@end
