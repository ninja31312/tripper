//
//  TPAddEventViewController.m
//  Tripper
//
//  Created by joehsieh on 3/17/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPAddEventViewController.h"
#import "MHTextField.h"
#import "TPEvent.h"
#import <Parse/PFObject.h>

@interface TPAddEventViewController ()
{
    UILabel *descriptionLabel;
    MHTextField *eventNameTextField;
    MHTextField *startDateTextField;
    MHTextField *endDateTextField;
}
@property (nonatomic, strong) MHTextField *eventNameTextField;
@property (nonatomic, strong) MHTextField *startDateTextField;
@property (nonatomic, strong) MHTextField *endDateTextField;
@property (nonatomic, strong) UILabel *descriptionLabel;
@end

@implementation TPAddEventViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
	view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.view = view;

    CGFloat paddingX = 10.0;
    CGFloat width = CGRectGetWidth(self.view.bounds) - 20.0;
    
    self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(paddingX, 84, width, 44.0)];
    self.descriptionLabel.font = [UIFont boldSystemFontOfSize:24.0];
    self.descriptionLabel.text = @"Create New Trip";
    self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.descriptionLabel];
    
    self.eventNameTextField = [[MHTextField alloc] initWithFrame:CGRectMake(paddingX, CGRectGetMaxY(descriptionLabel.frame) + 10.0, width, 44.0)];
    self.eventNameTextField.layer.borderWidth = 1.0;
    self.eventNameTextField.layer.borderColor = [UIColor grayColor].CGColor;
    self.eventNameTextField.textAlignment = NSTextAlignmentCenter;
    self.eventNameTextField.required = YES;
    [self.eventNameTextField setPlaceholder:@"Trip Name"];
    [self.view addSubview:self.eventNameTextField];
    
    self.startDateTextField = [[MHTextField alloc] initWithFrame:CGRectMake(paddingX, CGRectGetMaxY(eventNameTextField.frame) + 10.0, width, 44.0)];
    self.startDateTextField.layer.borderWidth = 1.0;
    self.startDateTextField.layer.borderColor = [UIColor grayColor].CGColor;
    self.startDateTextField.textAlignment = NSTextAlignmentCenter;
    self.startDateTextField.required = YES;
    [self.startDateTextField setDateField:YES];
    self.startDateTextField.dateFormat = @"yyyy-MM-dd";
    [self.startDateTextField setPlaceholder:@"Start Date"];
    [self.view addSubview:self.startDateTextField];
    
    self.endDateTextField = [[MHTextField alloc] initWithFrame:CGRectMake(paddingX, CGRectGetMaxY(startDateTextField.frame) + 10.0, width, 44.0)];
    self.endDateTextField.layer.borderWidth = 1.0;
    self.endDateTextField.layer.borderColor = [UIColor grayColor].CGColor;
    self.endDateTextField.textAlignment = NSTextAlignmentCenter;
    self.endDateTextField.required = YES;
    [self.endDateTextField setDateField:YES];
    self.endDateTextField.dateFormat = @"yyyy-MM-dd";
    [self.endDateTextField setPlaceholder:@"End Date"];
    [self.view addSubview:self.endDateTextField];
    
    // add the observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidChange:)
                                                 name:@"UITextFieldTextDidEndEditingNotification"
                                               object:nil];
}

- (void)textFieldDidChange:(NSNotification*)aNotification
{
    self.navigationItem.rightBarButtonItem.enabled = [eventNameTextField validate] && [startDateTextField validate] && [endDateTextField validate];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(_dismissPresentedViewController:)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(_addEvent:)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
	// Do any additional setup after loading the view.
}

- (void)_dismissPresentedViewController:(id)sender
{
    if (delegate) {
        [delegate addEventViewControllerDidCancel:self];
    }
}

- (void)_addEvent:(id)sender
{
    PFObject *event = [PFObject objectWithClassName:@"TPEvent"];
    event[@"eventName"] = self.eventNameTextField.text;
    event[@"startDate"] = self.startDateTextField.text;
    event[@"endDate"] = self.endDateTextField.text;
    [event saveInBackground];
    
    if (delegate) {
        [delegate addEventViewControllerDidComple:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@synthesize delegate;
@synthesize eventNameTextField;
@synthesize startDateTextField;
@synthesize endDateTextField;
@synthesize descriptionLabel;
@end
