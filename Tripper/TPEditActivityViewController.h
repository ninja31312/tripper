//
//  TPEditActivityViewController.h
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPActivity.h"
@class TPEditActivityViewController;

@protocol TPEditActivityViewControllerDelegate <NSObject>
- (void)editActivityViewControllerDidComple:(TPEditActivityViewController *)inViewController activity:(TPActivity *)inActivity;
@end

@interface TPEditActivityViewController : UIViewController
- (instancetype)initWithActivity:(TPActivity *)inActivity;
@property (nonatomic, assign) id<TPEditActivityViewControllerDelegate> delegate;
@end
