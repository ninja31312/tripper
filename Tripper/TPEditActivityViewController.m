//
//  TPEditActivityViewController.m
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPEditActivityViewController.h"
#import "MHTextField.h"
#import <Parse/PFObject.h>

@interface TPEditActivityViewController ()
{
    MHTextField *activiyNameTextField;
    MHTextField *noteTextField;
    TPActivity *activity;
}
@property (nonatomic, strong) MHTextField *activiyNameTextField;
@property (nonatomic, strong) MHTextField *noteTextField;
@property (nonatomic, strong) TPActivity *activity;
@end


@implementation TPEditActivityViewController

- (instancetype)initWithActivity:(TPActivity *)inActivity
{
    self = [super init];
    if (self) {
        self.activity = inActivity;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
	view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.view = view;
    
    CGFloat paddingX = 10.0;
    CGFloat width = CGRectGetWidth(self.view.bounds) - 20.0;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.activiyNameTextField = [[MHTextField alloc] initWithFrame:CGRectMake(paddingX,  70.0, width, 44.0)];
    self.activiyNameTextField.layer.borderWidth = 1.0;
    self.activiyNameTextField.layer.borderColor = [UIColor grayColor].CGColor;
    self.activiyNameTextField.textAlignment = NSTextAlignmentCenter;
    self.activiyNameTextField.required = YES;
    [self.activiyNameTextField setPlaceholder:@"Activity Name"];
    self.activiyNameTextField.text = activity.title;
    [self.view addSubview:self.activiyNameTextField];
    
    self.noteTextField = [[MHTextField alloc] initWithFrame:CGRectMake(paddingX, CGRectGetMaxY(activiyNameTextField.frame) + 10.0, width, 140.0)];
    self.noteTextField.layer.borderWidth = 1.0;
    self.noteTextField.layer.borderColor = [UIColor grayColor].CGColor;
    self.noteTextField.textAlignment = NSTextAlignmentCenter;
    [self.noteTextField setPlaceholder:@"Note"];
    self.noteTextField.text = activity.note;
    [self.view addSubview:self.noteTextField];
    
    // add the observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidChange:)
                                                 name:@"UITextFieldTextDidEndEditingNotification"
                                               object:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(_saveActivity:)];
    self.navigationItem.rightBarButtonItem.enabled = [activiyNameTextField validate];
}

- (void)_saveActivity:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(editActivityViewControllerDidComple:activity:)]) {
        [self.delegate editActivityViewControllerDidComple:self activity:activity];
    }
}

- (void)textFieldDidChange:(NSNotification*)aNotification
{
    self.navigationItem.rightBarButtonItem.enabled = [activiyNameTextField validate];
    activity.title = activiyNameTextField.text;
    activity.note = noteTextField.text;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@synthesize activiyNameTextField;
@synthesize noteTextField;
@synthesize activity;
@end
