//
//  TPEvent.h
//  Tripper
//
//  Created by joehsieh on 3/19/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "MTLModel.h"

@interface TPEvent : MTLModel
+ (TPEvent *)createEventWithDictionary:(NSDictionary *)inDict;
@property (nonatomic, copy) NSString *objectId;
@property (nonatomic, copy) NSString *eventName;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;
@end
