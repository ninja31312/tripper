//
//  TPEvent.m
//  Tripper
//
//  Created by joehsieh on 3/19/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPEvent.h"
#import "MTLValueTransformer.h"

@implementation TPEvent

+ (TPEvent *)createEventWithDictionary:(NSDictionary *)inDict
{
    TPEvent *event = [[TPEvent alloc] init];
    event.objectId = inDict[@"objectId"];
    event.eventName = inDict[@"title"];
    event.startDate = inDict[@"startDate"];
    event.endDate = inDict[@"endDate"];
    return event;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
	return @{
             @"objectId": @"objectId",
             @"eventName": @"eventName",
             @"startDate": @"startDate",
             @"endDate": @"endDate"
             };
}

+ (NSValueTransformer *)startDateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)endDateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    return dateFormatter;
}

@end
