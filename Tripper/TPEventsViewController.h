//
//  TPEventsViewController.h
//  Tripper
//
//  Created by joehsieh on 3/16/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionView.h"
#import "TPAddEventViewController.h"

@interface TPEventsViewController : UIViewController <UIScrollViewDelegate, PSCollectionViewDataSource, PSCollectionViewDelegate,TPAddEventViewControllerDelegate> {
    PSCollectionView *waterflowView;
}
@property (nonatomic, retain) PSCollectionView *waterflowView;
@end
