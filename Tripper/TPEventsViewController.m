//
//  TPEventsViewController.m
//  Tripper
//
//  Created by joehsieh on 3/16/14.
//  Copyright (c) 2014 TP. All rights reserved.
//


#import "TPEventsViewController.h"
#import "WaterflowViewCell.h"
#import "TPAddEventViewController.h"
#import <Parse/PFQuery.h>
#import "TPActivitiesViewController.h"

@interface TPEventsViewController ()

@property (nonatomic, strong) NSMutableArray *waterflowViewData;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@end

@implementation TPEventsViewController

@synthesize waterflowViewData = waterflowViewData;
@synthesize waterflowView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(_showAppInformationPage:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(_addEvent:)];
    
    self.waterflowView = [[PSCollectionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.waterflowView.delegate = self; // This is for UIScrollViewDelegate
    self.waterflowView.collectionViewDelegate = self;
    self.waterflowView.collectionViewDataSource = self;
    self.waterflowView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if ([[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
        self.waterflowView.numColsPortrait = 2;
        self.waterflowView.numColsLandscape = 4;
    } else {
        self.waterflowView.numColsPortrait = 2;
        self.waterflowView.numColsLandscape = 4;
    }
    
    [self.view addSubview:waterflowView];
    
    self.indicator = [[UIActivityIndicatorView alloc] init];
    indicator.color = [UIColor whiteColor];
    indicator.backgroundColor = [UIColor grayColor];
    
    CGFloat w = 44.0;
    CGFloat h = 44.0;
    indicator.frame = CGRectMake((CGRectGetWidth(self.view.frame) - w) / 2.0, (CGRectGetHeight(self.view.frame) - w) / 2.0, w, h);
    [self.view addSubview:indicator];
    
    [indicator startAnimating];
    [self loadData];
    
}

- (void)_showAppInformationPage:(id)sender
{
    
}

- (void)_addEvent:(id)sender
{
    TPAddEventViewController *vc = [[TPAddEventViewController alloc] init];
    vc.delegate = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request

- (void)loadData
{
    PFQuery *query = [PFQuery queryWithClassName:@"TPEvent"];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            NSLog(@"Error: %@", errorString);
            return;
        }
        NSMutableArray *result = [[NSMutableArray alloc] init];
        for (PFObject *item in objects) {
            NSString *timePeriodString = [NSString stringWithFormat:@"%@\n%@", item[@"startDate"], item[@"endDate"]];
            [result addObject:@{@"title":item[@"eventName"], @"timePeriod":timePeriodString ,@"width":@(320), @"height":@(320), @"startDate":item[@"startDate"], @"endDate":item[@"endDate"], @"objectId":item.objectId}];
        }
        self.waterflowViewData = result;
        [waterflowView reloadData];
        [indicator stopAnimating];
    }];
}

#pragma mark - PSCollection Delegate and DataSource

- (NSInteger)numberOfRowsInCollectionView:(PSCollectionView *)collectionView
{
    return [self.waterflowViewData count];
}

- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView cellForRowAtIndex:(NSInteger)index
{
    WaterflowViewCell *v = (WaterflowViewCell *)[waterflowView dequeueReusableViewForClass:nil];
    if (!v) {
        v = [[WaterflowViewCell alloc] initWithFrame:CGRectZero];
    }
    
    [v collectionView:waterflowView fillCellWithObject:self.waterflowViewData atIndex:index];
    
    return v;
}

- (CGFloat)collectionView:(PSCollectionView *)collectionView heightForRowAtIndex:(NSInteger)index
{
    NSDictionary *item = [self.waterflowViewData objectAtIndex:index];
    
    return [WaterflowViewCell heightForViewWithObject:item inColumnWidth:waterflowView.colWidth];
}

- (void)collectionView:(PSCollectionView *)collectionView didSelectCell:(PSCollectionViewCell *)cell atIndex:(NSInteger)index
{
    TPEvent *evnet = [TPEvent createEventWithDictionary:waterflowViewData[index]];
    TPActivitiesViewController *vc = [[TPActivitiesViewController alloc] initWithEvent:evnet];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark TPAddEventViewControllerDelegate

- (void)addEventViewControllerDidComple:(TPAddEventViewController *)inViewController
{
    [self dismissViewControllerAnimated:YES completion:^{
        [indicator startAnimating];
        [self loadData];
    }];
}

- (void)addEventViewControllerDidCancel:(TPAddEventViewController *)inViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@synthesize indicator;
@end
