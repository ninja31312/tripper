//
//  TPWebViewController.h
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPWebViewController : UIViewController <UIWebViewDelegate>
- (void)loadURL:(NSURL *)inURL;
@end
