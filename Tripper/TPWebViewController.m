//
//  TPWebViewController.m
//  Tripper
//
//  Created by joehsieh on 3/22/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "TPWebViewController.h"

@interface TPWebViewController ()
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@end

@implementation TPWebViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.webView = [[UIWebView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:webView];
        webView.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.indicator = [[UIActivityIndicatorView alloc] init];
    indicator.color = [UIColor whiteColor];
    indicator.backgroundColor = [UIColor grayColor];
    
    CGFloat w = 44.0;
    CGFloat h = 44.0;
    indicator.frame = CGRectMake((CGRectGetWidth(self.view.frame) - w) / 2.0, (CGRectGetHeight(self.view.frame) - w) / 2.0, w, h);
    [webView addSubview:indicator];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadURL:(NSURL *)inURL
{
    [indicator startAnimating];
    [webView loadRequest:[NSURLRequest requestWithURL:inURL]];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [indicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [indicator stopAnimating];
}

@synthesize webView;
@synthesize indicator;
@end
