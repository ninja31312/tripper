//
//  main.m
//  Tripper
//
//  Created by joehsieh on 3/16/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TPAppDelegate class]));
    }
}
