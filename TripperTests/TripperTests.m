//
//  TripperTests.m
//  TripperTests
//
//  Created by joehsieh on 3/16/14.
//  Copyright (c) 2014 TP. All rights reserved.
//

#import "Kiwi.h"
#import "TPAPI.h"

SPEC_BEGIN(APISpec)

NSNumber *(^_isContainKeyword) (NSString *) = ^NSNumber* (NSString *inKeyword) {
    NSArray *keywords = @[@"遊", @"玩", @"吃", @"美食"];
    for(NSString *keyword in keywords) {
        if([inKeyword rangeOfString:keyword].location != NSNotFound) {
            return @(YES);
        }
    }
    return @(NO);
};

//describe(@"Fetching article data", ^{
//    it(@"should get a article by a user in 10 seconds", ^{
//        __block id result = nil;
//        [[[TPAPI sharedAPI] retrieveHotArticlesForUser:@"giddens"] continueWithBlock:^id(BFTask *task) {
//            result = task.result;
//            NSLog(@"%@",task.result);
//            return task;
//        }];
//        [[expectFutureValue(result) shouldEventuallyBeforeTimingOutAfter(10)] beNonNil];
//    });
//});

//describe(@"Creating a article", ^{
//    it(@"should create a article in 10 seconds", ^{
//        __block id result = nil;
//        [[[TPAPI sharedAPI] createArticleWith:nil] continueWithBlock:^id(BFTask *task) {
//            result = task.result;
//            NSLog(@"%@",task.result);0
//            return task;
//        }];
//        [[expectFutureValue(result) shouldEventuallyBeforeTimingOutAfter(10)] beNonNil];
//    });
//});

describe(@"Searching articles by a keyword", ^{
    it(@"should search articles in 60 seconds", ^{
        __block id result = nil;
        [[[TPAPI sharedAPI] searchArticlesByKeyWord:@"花蓮"] continueWithBlock:^id(BFTask *task) {
            result = task.result;
            NSDictionary *result = task.result;
            for(NSDictionary *item in result[@"articles"]) {
                if([_isContainKeyword(item[@"site_category"]) boolValue] || [_isContainKeyword(item[@"category"]) boolValue]||
                   [_isContainKeyword(item[@"title"]) boolValue]) {
                    NSLog(@"title %@",item[@"title"]);
                    NSLog(@"category %@",item[@"category"]);
                    NSLog(@"site ategory %@",item[@"site_category"]);
                    NSLog(@"link %@",item[@"link"]);
                }
            }
            return task;
        }];
        [[expectFutureValue(result) shouldEventuallyBeforeTimingOutAfter(60)] beNonNil];
    });
});


SPEC_END
